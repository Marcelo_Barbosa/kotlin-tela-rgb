package com.example.aula

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.SeekBar
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), SeekBar.OnSeekBarChangeListener {

    private lateinit var  colorView: View
    private lateinit var  colorPickerR:SeekBar
    private lateinit var  colorPickerG:SeekBar
    private lateinit var  colorPickerB:SeekBar

    private  var  Rc: Int = 0
    private var  Gc: Int = 0
    private  var  Bc: Int = 0


    override fun onCreate(savedInstanceState: Bundle?)

    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        colorView = findViewById(R.id.main_view_color)
        colorPickerR = findViewById(R.id.main_seekbar_picker_R)
        colorPickerG = findViewById(R.id.main_seekbar_picker_G) 
        colorPickerB = findViewById(R.id.main_seekbar_picker_B)


        colorPickerR.setOnSeekBarChangeListener(this)
        colorPickerG.setOnSeekBarChangeListener(this)
        colorPickerB.setOnSeekBarChangeListener(this)

        colorView.setBackgroundColor(Color.BLACK)
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress:Int, fromUser: Boolean) {

        when(seekBar?.id){
            R.id.main_seekbar_picker_R->{Rc = progress}

            R.id.main_seekbar_picker_G->{Gc = progress}

            R.id.main_seekbar_picker_B->{Bc = progress}
        }




        colorView.setBackgroundColor(Color.argb(255,Rc,Gc,Bc))


    }




    override fun onStartTrackingTouch(seekBar: SeekBar?) {


    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {

    }

}